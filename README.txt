I used this project to learn concepts of Elliptic Curve Cryptography
The information is based on the book A Guide to Elliptic Curve Cryptography
    by Alfred Menezes, D.C. Hankerson, and Scott Vanstone
I used the GMP library for large number arithmetic
